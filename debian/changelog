tweak (3.02-6) unstable; urgency=medium

  * change test dependency from diff to diffutils

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 10 Mar 2020 09:22:55 -0400

tweak (3.02-5) unstable; urgency=medium

  * wrap-and-sort -ast
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * standards-version: bump to 4.5.0 (no changes needed)
  * use dh_missing --fail-missing

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 04 Mar 2020 13:35:40 -0500

tweak (3.02-4) unstable; urgency=medium

  [ Helmut Grohne ]
  * fix FTCBFS (closes: #911279)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 18 Oct 2018 22:12:32 -0400

tweak (3.02-3) unstable; urgency=medium

  * d/changelog: drop trailing whitespace
  * Standards-Version: bump to 4.2.1 (no changes needed)
  * use DEP-14 branch naming
  * gbp: avoid importing generated files

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 30 Aug 2018 12:27:19 -0400

tweak (3.02-2) unstable; urgency=medium

  * move to debhelper 11
  * d/control: add Rules-Requires-Root: no
  * d/control: move Vcs* to salsa.debian.org
  * Standards-Version: bump to 4.1.3 (priority extra→optional)
  * move upstream URLs to https
  * d/copyright: move to DEP-5
  * fixup https changes
  * patch Makefile to accept build variables from dh
  * d/rules: set all hardening build options
  * simple autopkgtest
  * include CPPFLAGS during build

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 13 Feb 2018 12:47:11 -0500

tweak (3.02-1) unstable; urgency=medium

  * New upstream release

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 15 Jul 2016 15:12:59 +0200

tweak (3.01-8) unstable; urgency=low

  * debian/control: bump Standards-Version to 3.9.3 (no changes needed),
    change Vcs- fields.
  * debian/rules: minimize, move to dh 8
  * convert source format to '3.0 (quilt)' (dropping dpatch build-dep)
  * drop halibut build-dep in favor of shipping upstream's man page and
    documentation directly.

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sun, 04 Mar 2012 19:37:53 -0500

tweak (3.01-7) unstable; urgency=low

  * Debian Packaging cleanup:
   - updated my e-mail address
   - moved to debhelper 7, minimized debian/rules
   - debian/control: remove XS- from Dm-Upload-Allowed
   - bumped policy to 3.8.1 (added README.source explaining dpatch)
   - copyright for debian packaging now references GPL-3 explicitly

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 30 Mar 2009 03:14:14 -0400

tweak (3.01-6) unstable; urgency=low

  * avoided common opportunities for buffer overflows (Closes: #469966).
  * switched doc-base section for tweak-btree to Programming.

 -- Daniel Kahn Gillmor <dkg-debian.org@fifthhorseman.net>  Sun, 16 Mar 2008 20:09:36 -0400

tweak (3.01-5) unstable; urgency=low

  * Adding upstream's svn r7478 to fix crashes with weird terminal sizes
    as root (Closes: #464628)

 -- Daniel Kahn Gillmor <dkg-debian.org@fifthhorseman.net>  Mon, 11 Feb 2008 23:51:46 -0500

tweak (3.01-4) unstable; urgency=low

  * added XS-Dm-Upload-Allowed:, Homepage:, Vcs-Browser: Vcs-Svn: fields
    to debian/control
  * changed debian packaging license to GPL v3 or later
  * update standards version to 3.7.3 (changed menu)

 -- Daniel Kahn Gillmor <dkg-debian.org@fifthhorseman.net>  Wed, 05 Dec 2007 00:14:35 -0500

tweak (3.01-3) unstable; urgency=low

  * switched to using dh_install and dh_links.
  * removed Rafael Laboissiere from Uploaders, since he's just the sponsor
    and not a co-maintainer.
  * added doc-base to index Simon's btree.html

 -- Daniel Kahn Gillmor <dkg-debian.org@fifthhorseman.net>  Thu, 15 Mar 2007 14:59:41 -0400

tweak (3.01-2) unstable; urgency=low

  * removed debian/dirs
  * cleaned up debian/rules and added proper copyright/license
  * debian/control: added rafael to uploaders, dropped ${misc:Depends},
    properly formatted URL and added another short paragraph.
  * fixed up menu entry with tweak-wrapper
  * added btree.html to docs
  * thanks, Rafael Laboissiere!

 -- Daniel Kahn Gillmor <dkg-debian.org@fifthhorseman.net>  Wed, 14 Mar 2007 15:50:31 -0400

tweak (3.01-1) unstable; urgency=low

  * Initial release (Closes: #414844)

 -- Daniel Kahn Gillmor <dkg-debian.org@fifthhorseman.net>  Tue, 13 Mar 2007 21:44:07 -0400
